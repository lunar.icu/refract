<?php
$time_start = microtime(true);
include 'loader.php';
if ($session->get('lang')) {
    if ($route->checkLang($session->get('lang'))) {
        foreach ($route->getLangFiles($route->checkLang($session->get('lang'))) as $value) {
            include $value;
        }
    }
} else {
    foreach ($route->getLangFiles($route->getDefaultLang()) as $value) {
        include $value;
    }
}

//website exceptional files
$exceptions = $route->getException();
//website root folder
$baseFolder = $route->getBaseFolder();
//Website base url
$baseUrl = $route->checkProtocol().$_SERVER["HTTP_HOST"].$baseFolder;
//võtab urli parameetrid
$request = str_replace($baseFolder, "/", $_SERVER['REQUEST_URI']);
$request2 = substr($request, 1, strlen($request));
$req = explode('/', $request2);
//normalized request data
$normalizedData = $route->normalize($req);
$requestDir = $normalizedData['requestDir'];
$req = $normalizedData['request'];
$normalRequest = $normalizedData['normalRequest'];
$dirPath = $normalizedData['pageFolder'];

//check if main file is exceptional file
if (isset($exceptions[$requestDir.$normalRequest])) {
    foreach ($exceptions[$requestDir.$normalRequest] as $path) {
        if (file_exists($path)) {
            include $path;
        }
    }
} else {
    //Checks if main fail exists
    if (file_exists($dirPath.$req[0].".php")) {
        //Before main file files
        $headers = $route->getBefore($requestDir, $normalRequest);
        if ($headers !== null) {
            foreach ($headers as $header) {
                include $dirPath.$header;
            }
        }
        //load main file
        include $dirPath.$req[0].".php";
        //After main file files
        $footers = $route->getAfter($requestDir, $normalRequest);
        if ($footers !== null) {
            foreach ($footers as $footer) {
                include $dirPath.$footer;
            }
        }
    } else { //if main file doesn't
        $errors = $route->getErrors(null,'404');
        if ($errors) {
            include $dirPath.$route->getErrors($requestDir, '404');
        } else {
            die('404');
        }
    }
}
$time_end = microtime(true);
$runtime = ($time_end - $time_start);