<?php

$login['Normal']['table'] = 'users';
$login['Normal']['passwordTable'] = 'password';
$login['Normal']['authTable'] = 'username';
$login['Normal']['encryptOptions']['cost'] = 12;
$login['Normal']['name'] = 'nickname';
$login['Normal']['requiredData']['uid'] = 'uid';
$login['Normal']['requiredData']['name'] = 'nickname';
$login['Normal']['requiredData']['lang'] = 'lang';


$login['Team']['table'] = 'teams';
$login['Team']['passwordTable'] = 'password';
$login['Team']['authTable'] = 'name';
$login['Team']['requiredData']['tid'] = 'tid';
$login['Team']['requiredData']['members'] = 'members';
$login['Team']['name'] = 'name';