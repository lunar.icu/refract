<?php


//before main faile files
$route['before']['/'][2] = 'header.php';
$route['before']['/'][3] = 'navbar.php';
$route['before']['/leflex/test'][1] = 'index2.php';
$route['before']['/no/yes/margus/'][0] = 'header.php';

$route['before']['/admin/'][0] = 'header.php';
$route['before']['/admin/'][1] = 'navbar.php';
$route['after']['/admin/'][1] = 'footer.php';


$route['before']['/le/'][0] = 'header.php';
$route['after']['/le/'][0] = 'footer.php';
//after main file files
$route['after']['/'][0] = 'footer.php';

//error pages
$route['error']['/']['404'] = '404.php';

//exceptional files (will start from root folder)
$route['exception']['/process'][] = 'process.php';
$route['exception']['/ajax'][] = 'ajax.php';
$route['exception']['/game'][] = 'views/game.php';
$route['exception']['/addMember'][] = 'views/addMember.php';

$route['exception']['/test'][] = 'test.php';
$route['exception']['/admin/login'][] = 'views/admin/login.php';

//default page file (if req is empty.) (filename without extension)
$route['basePage'] = 'index';

//webpage defualt file location
$route['routeFolder'] = 'views';

//load before and after main files on error
$route['options']['error']['loadFiles'] = false;

//base language
$route['options']['language']['default'] = 'est';

//languages base folder
$route['options']['language']['folder'] = 'inc/languages/';

$route['dir']['classes'] = 'inc/classes/';
$route['dir']['functions'] = 'inc/functions/';
$route['dir']['login'] = 'inc/login/';