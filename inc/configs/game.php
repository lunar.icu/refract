<?php

$game['table'] = 'game';
$game['table_locations'] = 'game_locations';
$game['table_gameTeams'] = 'game_teams';
$game['table_team'] = 'teams';
$game['table_startFin'] = 'game_startFin';
$game['table_questions'] = 'game_question';
$game['table_status'] = 'game_status';
$game['table_answers'] = 'game_answers';