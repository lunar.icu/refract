<?php

class Normal extends Database
{

    private $table;
    private $passwordTable;
    private $authTable;
    private $options;

    public function __construct()
    {
        parent::__construct();
        $config = new Config();
        $data = (object) $config->config['login'][get_class($this)];
        $this->table = $data->table;
        $this->passwordTable = $data->passwordTable;
        $this->authTable = $data->authTable;
        $this->options = $data->encryptOptions;


    }

    public function loginCheck($data) {
        $authName = array_shift($data);
        $password = array_shift($data);
        $data =  $this->select("SELECT * FROM $this->table WHERE $this->authTable = '". $authName ."';");
        if ($data) {
            if (password_verify($password, $data[$this->passwordTable])) {
                echo 'fdsa';
                return $data;
            }
        }
        return false;
    }

    public function register($data) {
        $authName = array_shift($data);
        $password = $this->passwordEncrypt(array_shift($data));
        $this->insert($this->table, ['username'=> $authName, 'password' => $password, 'nickname' => $authName, 'lang' => 'est']);
        return true;
    }

    public function passwordEncrypt($password)
    {
        return password_hash($password, PASSWORD_BCRYPT, $this->options);
    }

}