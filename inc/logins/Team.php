<?php

class Team extends Database{
    private $table;
    private $passwordTable;
    private $authTable;
    private $table_gameTeams;

    public function __construct()
    {
        parent::__construct();
        $config = new Config();
        $data = (object) $config->config['login'][get_class($this)];
        $game = (object) $config->config['game'];
        $this->table = $data->table;
        $this->passwordTable = $data->passwordTable;
        $this->authTable = $data->authTable;
        $this->table_gameTeams= $game->table_gameTeams;

    }

    public function loginCheck($data) {
        $authName = array_shift($data);
        $password = array_shift($data);


        $data =  $this->select("SELECT * FROM $this->table WHERE $this->authTable = '". $authName ."';");

        $game_check = $this->select("SELECT * FROM $this->table t INNER JOIN $this->gable_gameTeams gt ON gt.tid = t.tid WHERE t.name =  '". $authName ."';");
        if (!$game_check) {
            return false;
        }
        if ($data) {
            if ($password == $data[$this->passwordTable]) {
                return $data;
            }
        }
        return false;
    }

    public function addMembers($data) {
        $members = $data['members'];
        $tid = $data['tid'];
        $this->update($this->table, ['members' => $members], "tid = '$tid'");
        return true;
    }

    public function register($data) {
        $authName = array_values($data)[0];
        $sqlcheck ="SELECT * FROM $this->table WHERE $this->authTable = '". $authName ."';";
        if(!$this->select($sqlcheck)) {
            $this->insert($this->table, $data);
            return true;
        }
        return false;
    }
}