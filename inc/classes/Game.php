<?php
class Game extends Database {

    private $table;
    private $table_team;
    private $table_gameTeams;
    private $table_locations;
    private $table_startFin;
    private $table_status;
    private $table_questions;
    private $table_answers;

    function __construct()
    {
        parent::__construct();
        $config = new Config();
        $data = (object) $config->config['game'];
        $this->table = $data->table;

        $this->table_team = $data->table_team;
        $this->table_gameTeams = $data->table_gameTeams;
        $this->table_locations = $data->table_locations;
        $this->table_startFin = $data->table_startFin;
        $this->table_status = $data->table_status;
        $this->table_questions = $data->table_questions;
        $this->table_answers = $data->table_answers;
    }

    function create($startFin, $markers, $teams, $gameName) {


        if ($gameName === '' || $startFin === null || sizeof($markers) <= 1 || sizeof($teams) === 0) {
            return false;
        }
        $gameId = $this->insert('game', ['gamename' => $gameName, 'startFin' => $startFin]);
        $location_ids = [];
        foreach ($markers as $key =>  $value) {
            $obj = (object) $value;
            $questionId = $this->insert('game_question', ['question' => $obj->question, 'answerType' => $obj->answerTypeVal, "$obj->answerTypeVal" => (is_array($obj->answer) ? json_encode($obj->answer) : $obj->answer)]);
            $locationID = $this->insert('game_locations', ['lat' => $obj->lat, 'lng' => $obj->lng, 'qid' => $questionId]);
            $location_ids[] = $locationID;
        }


        print_r($location_ids);
        foreach ($teams as $key => $value) {
            print_r($value);
            $this->insert('game_teams', ['gameid' => $gameId, 'tid' => $value]);
            foreach ($location_ids as $k => $v) {
                print_r($v);
                $this->insert('team_locations', ['tid' => $value, 'glid' => $v]);
            }
            $last_key = key($location_ids);
            $last_value = array_pop($location_ids);
            $location_ids = array_merge(array($last_key => $last_value), $location_ids);
        }
        return true;
    }

    function purge() {

    }

    function getStatus() {

    }

    function getTeams() {
        return $teams = $this->select("SELECT t.tid AS tid, t.name AS name, t.members AS members, g.gameid AS gameid, g.gamename AS gamename FROM teams t LEFT JOIN game_teams gt ON t.tid = gt.tid LEFT JOIN game g ON gt.gameid = g.gameid");
    }

    function getGame($tid) {
        $started = false;
        $ended = false;
        $result = $this->select("select * from teams JOIN game_teams on teams.tid = $tid AND game_teams.tid = $tid");
        if ($result['gameid'] == null) {
            return false;
        }
        $gameid = $result['gameid'];
        $startFin = $this->select("SELECT startFin FROM $this->table WHERE gameid = $gameid");
        $status = (array) $this->select("SELECT * FROM $this->table_status WHERE tid = $tid AND gameid = $gameid");


        $team_locations = $this->select("SELECT gl.glid AS glid, gl.lat AS lat, gl.lng AS lng, gq.qid AS qid, gq.question AS question, gq.answerType AS answerType, gq.answerWord AS answerWord, gq.answerNumber AS answerNumber, gq.answerSelection AS answerSelection FROM team_locations tl INNER JOIN game_locations gl ON tl.tid = $tid AND tl.glid = gl.glid LEFT JOIN game_question gq ON gq.qid = gl.qid WHERE NOT EXISTS (SELECT * FROM game_answers WHERE game_answers.tid = tl.tid AND game_answers.glid = gl.glid)", true);
        if (isset($status['start_time']) and $status['start_time'] != null) {
            $started = true;
        }
        if (isset($status['end_time']) and $status['end_time'] != null) {
            $ended = true;
        }



        $data['startFin'] = json_decode($startFin);
        $data['markers'] = $team_locations;
        $data['started'] = $started;
        $data['ended'] = $ended;
        $data['gameid'] = $gameid;




        return $data;

    }

    function start($tid, $gameid) {
        $check = $this->select("SELECT * FROM $this->table_status WHERE tid = $tid AND gameid = $gameid");
        if ($check) {
            return false;
        }
        $id = $this->insert($this->table_status, ['tid' => $tid, 'gameid' => $gameid]);
        if ($id) {
            return true;
        }
        return false;

    }

    function save($tid, $gameid, $glid, $answer, $answer_boolean = null) {

        $data = ['tid' => $tid, 'gameid' => $gameid, 'glid' => $glid, 'answer' => $answer ];
        if ($answer_boolean !== null) {
            $data['answer_boolean'] = $answer_boolean;
        }
        $result = $this->insert($this->table_answers, $data);
        if ($result === null) {
            return false;
        }
        return true;

    }

    function end($tid, $gameid) {
        $result = $this->update($this->table_status, ['end_time' => 'NOW()'], "`tid` = '$tid' AND `gameid` = '$gameid'");
        return $result;
    }

    function countGames() {
        $result = $this->select("SELECT count(*) AS total FROM $this->table");
        return $result;
    }

    function countTeams() {
        $result = $this->select("SELECT count(*) AS total FROM $this->table_team");
        return $result;
    }

    function gameList() {
        $result = $this->select("SELECT g.gameid AS gameid, g.gamename AS gameName, GROUP_CONCAT(t.name SEPARATOR ',') AS teamName FROM $this->table_gameTeams gt INNER JOIN $this->table g ON gt.gameid = g.gameid INNER JOIN $this->table_team t ON t.tid = gt.tid GROUP BY g.gameid");
        return $result;
    }

    function gameStatus($gameid) {
        $gamename = $this->select("SELECT gamename FROM $this->table WHERE gameid = $gameid");
        if (empty($gamename)) {
            return false;
        }
        $teams = $this->select("SELECT t.tid AS tid, t.name AS name, t.members AS members FROM game_teams gt INNER JOIN teams t ON t.tid = gt.tid AND gt.gameid = $gameid", true);

        $teams_locations = [];
        $tid = 0;
        $ended = true;
        $counter = 0;

        foreach ($teams as $k => $v) {
            $tid = $v['tid'];
            $needsCheck = false;
            $teamname = $v['name'];
            $teams_locations[$teamname]['p'] = $this->select("SELECT gl.glid AS glid, ga.glid AS gaglid, ga.gsid AS gsid, ga.time AS time, ga.answer AS userAnswer, ga.answer_boolean AS answer_boolean, gq.question AS question, COALESCE( gq.AnswerSelection, gq.answerNumber, gq.answerWord ) AS answer FROM team_locations tl INNER JOIN game_locations gl ON tl.tid = $tid AND tl.glid = gl.glid LEFT JOIN game_question gq ON gq.qid = gl.qid LEFT JOIN game_answers ga ON ga.glid = gl.glid AND ga.tid = $tid", true);
            $needsCheckResult = $this->select("SELECT * FROM $this->table_answers WHERE tid = $tid AND answer_boolean IS NULL");
            $needsCheck = !empty($needsCheckResult) ? true : false;
            print_r($needsCheck);
            $status = $this->select("SELECT * FROM $this->table_status WHERE tid = $tid");
            if ($status != null) {
                if (isset($status['end_time']) && $status['end_time'] !== null) {
                    $teams_locations[$teamname]['status'] = "Lõpetatud";

                }elseif (isset($status['start_time']) && $status['start_time'] !== null) {
                    $teams_locations[$teamname]['status'] = "Käib";
                    $ended = false;
                }
            } else {
                $teams_locations[$teamname]['status'] = "Pole alustanud";
                $ended = false;
            }
            $teams_locations[$teamname]['check'] = $needsCheck;
            $counter = sizeof($teams_locations);
        }
        $data['gameName'] = $gamename;
        $data['team'] = $teams_locations;
        $data['count'] = $counter + 1;
        $data['ended'] = $ended;




        return $data;
    }

    function updateAnswer($data) {
        foreach ($data as $key => $val) {
            $gsid = $val['gsid'];
            $this->update($this->table_answers, ['answer_boolean'  => $val['answer_boolean']], "gsid = $gsid");
        }
        return true;
    }
}