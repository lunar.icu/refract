<?php

class Database extends Config
{

    private $mysqli;

    private $db;
    private $host;
    private $username;
    private $password;
    private $database;
    private $port;

    public function __construct()
    {
        parent::__construct();

        $this->db = $this->config['database'];
        $this->host = $this->db['host'];
        $this->username = $this->db['username'];
        $this->password = $this->db['password'];
        $this->database = $this->db['database'];
        $this->port = $this->db['port'];
        
        //test connection
        $this->testConnection();
    }

    
    public function connect() {
        $this->mysqli = new mysqli($this->host, $this->username, $this->password, $this->database);
        if ($this->mysqli->connect_errno) {
            die('Connect Error: ' . $this->mysqli->connect_error);
        }
        $this->mysqli->set_charset("utf8");

    }

    public function disconnect()
    {
        if ($this->mysqli) {
            $this->mysqli->close();
        }
    }

    public function testConnection() {
        try {
            $this->connect();
            $this->disconnect();
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        return true;
    }

    public function sql($sql, $lastID = false) {
        $this->connect();
        $this->mysqli->query($sql);
        
        $connData =  $this->mysqli;
        $error = $this->mysqli->error;
        
        if ($error) {
            $this->disconnect();
            $meh =  "$error \n SQL : $sql";
            throw new $meh;
        }
        if ($lastID) {
            $lastid = $this->mysqli->insert_id;
            $this->disconnect();
            return $lastid;
        }
        $this->disconnect();
        return true;
    }

    /*
     * $query = normal sql select statement to get data;
     * $multi = force use multi dimensional array;
     * return types ---
     * String - When there is only one data
     * Array - when there is only one row of data
     * multi dim array - When there is more than one row of data
     * false - if there is no data
     */
    public function select($query, $multi = false) {
        $res = array(); // creating empty array variable
        $this->connect(); // connecting to database
        $result = $this->mysqli->query($query); //getting data from database
        $this->disconnect();
        if ($result) { // checks if data is not empty
            $rows_count = $result->num_rows; // counting data rows
            if ($rows_count == 1 && !$multi) { // if data rows is equal to one or force multi dimensional is not enabled
                $data = $result->fetch_assoc(); // query data to array
                if (count($data) == 1) { // checking if there is more than one data
                    return implode("", $data); // converting to string
                }

                return $data; // returning as single array
            }
            while ($r = $result->fetch_assoc()) { // when there is more than one data row
                $res[] = $r; // adding data to multi dim array
            }
            return $res; // returns as multi dimensional array

        }

        return false; //returns false if there is no data
    }

    public function delete($table, $where = "") {
        $sql = "DELETE FROM `$table`";
        if ($where) {
            $sql .= " where $where";
        }
        $this->sql($sql);
        return true;
    }

    public function insert($table, $data) {
        $sql = "INSERT INTO $table(";
        foreach ($data as $i => $k) {
            $sql .= "`" . $i . "`" . ", ";
        }
        $sql = substr($sql, 0, - 2);
        $sql .= ") VALUES(";
        foreach ($data as $i => $k) {
            $sql .= "'" . $k . "'" . ", ";
        }
        $sql = substr($sql, 0, - 2);
        $sql .= ")";
        return $this->sql($sql, true);
    }

    public function update($table, $data, $condition) {
        $sql = "UPDATE $table SET ";
        foreach ($data as $i => $k) {
            $sql .= "`" . $i . "`=";
            $sql = substr(trim($k), -2) === "()" ? $sql . $k . ", " : $sql . "'" . $k . "', ";


        }
        $sql = substr($sql, 0, - 2);
        $sql .= " WHERE $condition";
        $this->sql($sql);

        return true;

    }

    public function arrayToJson($array) {
        return json_encode($array);
    }

    public function JsonToArray($json) {
        return json_decode($json, true);
    }

    public function createDatabase($database) {
        
        if (!$this->checkIfDatabaseExists($database)) {
            if($this->sql("CREATE DATABASE IF NOT EXISTS $database")) {
                return true;
            }
            return 'fda';
        }
        return false;
    }


    public function dropDatabase($database) {
        if($this->checkIfDatabaseExists($database)) {
            $sql = "DROP DATABASE $database";
            if($this->sql($sql)) {
                return true;
            }
            return false;
        }
        return false;
    }

    
    public function exportDB($database = null) {

    }

    public function checkIfDatabaseExists($database) {
        $this->connect();
        $result = $this->mysqli->query("SHOW DATABASES LIKE '$database'");
        $this->disconnect();
        if ($result->num_rows > 0) {
            return true;
        }
        return false;
    }

    public function checkIfTableExists($table) {
        $this->connect();
        $result = $this->mysqli->query("SELECT table_name FROM information_schema.tables WHERE table_schema = '{$this->database}' AND table_name = '$table';");
        $this->disconnect();
        if ($result->num_rows > 0) {
            return true;
        }
        return false;

    }

    public function getVersion()
    {
        $this->connect();
        $version = $this->mysqli->server_info;
        $this->disconnect();
        return $version;
    }
}
