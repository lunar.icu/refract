<?php

class Permission extends Database{

    public function __construct()
    {
        parent::__construct();
    }

    /*
     * $perm = Slang permission name
     * $desc = permission description
     * $ptid = Permission type id (optional if you don't want to use permission groups
     */
    public function setPermissions($perm, $desc, $ptid = null) {
        echo $sql = "SELECT * FROM permissions WHERE permissions = '{$desc}'";

    }

    /*
     * $name - group name
     * $desc - information about this group
     * $level - the lower level is the higher ranked group is
     * /// Return errors ///
     * false - group already exists
     * /////
     * return true - group is created
     */
    public function setGroup($name, $desc, $level) {
        $checker = $this->select("SELECT * FROM groups WHERE name = '$name'");
        if ($checker) {
            return false;
        }
        $data['name'] = $name;
        $data['description'] = $desc;
        $data['level'] = $level;
        $this->insert("groups", $data);
        return true;
    }

    public function setPermissionToGroup() {

    }

    public function setPermissionType() {

    }

    public function setPermission($permission, $desc, $ptid = null) {
        $checker = $this->select("SELECT * FROM permissions WHERE permission = '$permission'");
        if ($checker) {
            return false;
        }
        $data['permission'] = $permission;
        $data['description'] = $desc;
        if ($ptid != null) {
            $data['ptid'] = $ptid;
        }
        $this->insert("permissions", $data);

    }

    public function permissions() {
        return $this->select("SELECT * FROM permissions", true);
    }

    public function groups() {
        return $this->select("SELECT * FROM groups", true);
    }

    public function permissionTypes() {
        return $this->select("SELECT * FROM permission_types", true);
    }

    public function typePermissions()
    {
        $types = $this->permissionTypes();
        $perms = $this->permissions();

        return $types;
        
    }

    public function userPermissions($uid) {
        $gid = $this->select("SELECT gid FROM users WHERE uid = $uid");
        $groupPermissions = $this->groupPermissions($gid);
        return $groupPermissions;
    }

    public function checkPermission($perm, $uid) {

    }

    public function groupPermissions($gid) {
        $permissions = $this->select("SELECT * FROM group_permissions WHERE gid = $gid", true);
        return $permissions;
    }


}
