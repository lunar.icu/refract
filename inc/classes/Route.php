<?php

class Route extends Config
{
    private $route;
    private $before;
    private $after;
    private $basePage;
    private $routeFolder;
    private $exception;
    private $error;
    private $option;
    public $dir;

    public function __construct()
    {
        parent::__construct();
        if ($this->config['route']) {
            $this->route = $this->config['route'];
        }
        if (isset($this->route['before'])) {
            $this->before = $this->route['before'];
            ksort($this->before);
        }
        if (isset($this->route['after'])) {
            $this->after = $this->route['after'];
            ksort($this->after);
        }
        if (isset($this->route['basePage'])) {
            $this->basePage = $this->route['basePage'];
        } else {
            die('No default file is set!');
        }
        if (isset($this->route['routeFolder'])) {
            $this->routeFolder = $this->route['routeFolder'];
        } else {
            die('no route folder is set!');
        }
        if (isset($this->route['exception'])) {
            $this->exception = $this->route['exception'];
        }
        if (isset($this->route['error'])) {
            $this->error = $this->route['error'];
        }
        if (isset($this->route['options'])) {
            $this->option = $this->route['options'];
        }
        if (isset($this->route['dir'])) {
            $this->dir = $this->route['dir'];
        }


    }

    public function getBefore($route = null, $page = null) {
        if (!is_null($route) && !is_null($page)) {
            $request = $route.$page;
            if (isset($this->before[$route])) {
                $tmpAry = $this->before[$route];
            }

            if (isset($this->before[$request])) {
                foreach ($this->before[$request] as $key => $value) {
                    $tmpAry[$key] = $value;
                }
            }
            if (isset($tmpAry)) {
                ksort($tmpAry);
                return $tmpAry;
            }

            return null;

        } else if (!is_null($route)) {
            if (isset($this->before[$route])) {
                return $this->before[$route];
            }
            return null;
        }
        return $this->before;
    }

    public function getAfter($route = null, $page = null) {
        if (!is_null($route) && !is_null($page)) {
            $request = $route.$page;
            if (isset($this->after[$route])) {
                $tmpAry = $this->after[$route];
            }

            if (isset($this->after[$request])) {
                foreach ($this->after[$request] as $key => $value) {
                    $tmpAry[$key] = $value;
                }
            }
            if (isset($tmpAry)) {
                ksort($tmpAry);
                return $tmpAry;
            }

            return null;

        } else if (!is_null($route)) {
            if (isset($this->after[$route])) {
                return $this->after[$route];
            }
            return null;
        }
        return $this->after;
    }

    public function getBasePage() {
        return $this->basePage;
    }

    public function getRouteFolder() {
        return $this->routeFolder;
    }

    public function getException() {
        return $this->exception;
    }

    public function normalize($request) {
        $tmpDir = $this->routeFolder.'/';
        $pageFolder = $this->routeFolder.'/';
        $dir = "/";
        foreach ($request as $folderChecker) {
            $tmpDir .= $folderChecker.'/';
            if (is_dir($tmpDir) && !empty($folderChecker)) {
                $pageFolder .= $folderChecker.'/';
                $dir .= $folderChecker.'/';
                array_shift($request);
            } else {
                break;
            }
        }
        $numberOfRequests = count(array_keys($request));
        if ($numberOfRequests > 1) {
            $key = array_key_last($request);
            if (empty($request[$key])) {
                unset($request[$key]);
            }
        } else {
            if (empty($request) or empty($request[0])) {
                $request[0] = $this->basePage;
            }
        }

        $data['pageFolder'] = $pageFolder;
        $data['request'] = $request;
        $data['normalRequest'] = implode('/', $request);
        $data['requestDir'] = $dir;
        return $data;
    }

    public function getErrors($request = null, $error = null) {
        if (!is_null($request) && !is_null($error)) {
            if (isset($this->error[$request][$error])) {
                return $this->error[$request][$error];
            }
            return false;
        }
        return $this->error;
    }

    public function checkProtocol() {
        if(isset($_SERVER['HTTPS'])) {
            return "https://";
        } else {
            return "http://";
        }
    }

    public function getBaseFolder() {
        $self = $_SERVER['SCRIPT_NAME'];
        $selfFilename = basename($_SERVER['SCRIPT_FILENAME']);
        return str_replace($selfFilename, "", $self);
    }

    public function loadErrorFiles() {
        return $this->option['error']['loadFiles'];
    }

    public function getLangOptions() {
        foreach ((glob($this->option['language']['folder'].'*')) as $dir) {
            $langs[] = basename($dir);
        }
        return $langs;
    }

    public function getLangDir($lang = null) {
        if ($lang != null) {
            return $this->option['language']['folder'].$lang.'/';
        }
        return $this->option['language']['folder'];
    }

    public function getDefaultLang() {
        return $this->option['language']['default'];
    }

    public function getLangFiles($lang) {
        if ($this->checkLang($lang)) {
            $langDir = $this->getLangDir($lang);
            return (glob($langDir.'*'));
        }
        return false;
    }

    public function checkLang($lang) {
        $langs = $this->getLangOptions();
        if (in_array($lang, $langs)) {
            return true;
        }
        return false;
    }

    public function data($data) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
}
