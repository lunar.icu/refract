<?php

class Session
{
    public function __construct()
    {
        session_start();
    }

    public function add($key, $value, $child = null)
    {
        if (!is_null($child)) {
            if(!isset($_SESSION[$key])) {
                $_SESSION[$key] = null;
            }
            $_SESSION[$key][$child] = $value;
        } else {
            $_SESSION[$key] = $value;
        }

    }

    public function get($key = null, $child = null)
    {
        if ($key == null && $child == null) {
            return $_SESSION;
        }
        if ($child) {
            if (isset($_SESSION[$key][$child])) {
                return $_SESSION[$key][$child];
            }
            return ;
        } else {
            if (isset($_SESSION[$key])) {
                return $_SESSION[$key];
            }
        }
        
        return false;

    }

    public function delete($key, $child = null)
    {
        if ($child) {
            unset($_SESSION[$key][$child]);
        }
        unset($_SESSION[$key]);
    }

    public function clear()
    {
        session_unset();
    }

    public function printAll() {
        echo '<br><pre>';
        print_r($_SESSION);
        echo '</pre><br>';
    }
}