<?php

class LoginManager extends Config {

    public $types;
    private $loginDir;
    private $lm;
    public $loginConfig;

    public function __construct()
    {
        parent::__construct();
        if (isset($this->config['lm'])) {
            $this->lm = $this->config['lm'];
            $this->loginDir = $this->lm['loginDir'];
        } else {
            die("Login classes directory is not set");
        }
        if (isset($this->config['login'])) {
            $this->loginConfig = $this->config['login'];
        } else {
            die("No login config data");
        }
    }

    public function getLoginTypes() {
        if (!is_dir($this->loginDir)) {
            return false;
        }
        $loginClasses = glob("$this->loginDir*");
        if (count($loginClasses) > 0) {
            foreach ($loginClasses as $value) {
                $filename =  basename($value, ".php");
                include_once $value;
                $class = null;
                try {
                    $class = new ReflectionClass($filename);
                } catch (ReflectionException $e) {
                }
                $methods = $class->getMethods();
                $this->types[$filename] =  null;
                foreach ($methods as $obj) {
                    if ($obj->class == $filename) {
                        $this->types[$filename][] = $obj->name;
                    }
                }
            }
            return $this->types;
        }
        return false;
    }

    public function request($type, $function, $data) {
        $this->getLoginTypes();
        if (!array_key_exists($type, $this->types)) {
            return false;
        }
        $loginClass = new $type();
        if (method_exists($loginClass, $function)) {
            return $loginClass->$function($data);
        }
        return false;

    }
}