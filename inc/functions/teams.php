<?php

function getTeams() {
    global $db;
    $sql = "SELECT * FROM teams";
    return $db->select($sql, true);
}

function getAvailableTeams() {
    global $db;
    $sql = "SELECT * FROM  teams WHERE  NOT EXISTS (SELECT * FROM  game_teams WHERE  game_teams.tid = teams.tid)";
    return $db->select($sql, true);
}