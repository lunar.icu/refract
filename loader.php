<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

spl_autoload_register(function ($class_name) {
    $class_name .= '.php';
    if (file_exists('inc/classes/'.$class_name)) {
        include 'inc/classes/'.$class_name;
    } elseif(isset($route)) {
        foreach ($route->dir['classes'] as $key => $value) {
            echo $key;
            if (file_exists($route->route['dir']['classes'].$class_name)) {
                include $route->dir['classes'].$class_name;
            }
        }
    }
});

$config = new Config();
$route = new Route();
$db = new Database();
$session = new Session();
$perm = new Permission();
$lm = new LoginManager();
$game = new Game();
$functions = array_values(array_diff(scandir($route->dir['functions']), array('.', '..')));
foreach ($functions as $fun) {
    include $route->dir['functions'].$fun;
}