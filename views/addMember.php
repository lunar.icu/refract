<?php
if($_SESSION['members']) {
    header('Location: /game');
} ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?= $baseUrl ?>" />
    <title>Refract - Login</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/css/bootstrap/bootstrap.min.css">
</head>
<body>
<div class="container center">
    <h1 class="mb-4 mt-4">Lisa meeskonnad</h1>
    <form action="/process" method="post">
        <div class="form-group">
            <input type="text" name="group[]" class="form-control" value="" placeholder="Täis nimi" required>
        </div>
        <div class="form-group">
            <input type="text" name="group[]" class="form-control" value="" placeholder="Täis nimi" required>
        </div>
        <div class="form-group">
            <input type="text" name="group[]" class="form-control" value="" placeholder="Täis nimi">
        </div>
        <div class="form-group">
            <input type="text" name="group[]" class="form-control" value="" placeholder="Täis nimi">
        </div>
        <div class="form-group">
            <input type="text" name="group[]" class="form-control" value="" placeholder="Täis nimi">
        </div>
        <div class="">
            <button type="button" id="addinput" class="btn btn-danger" name="button">Lisa veerg</button>
            <input type="hidden" name="process" value="addMembers">
            <input type="hidden" name="LoginType" value="Team">
            <button type="submit" class="btn btn-primary" name="button">Lisa meeskond</button>
        </div>
    </form>

</div>
<script src="/static/js/jquery/jquery.min.js"></script>
<script>
    $(document).on('click', '#addinput', function(){
        var html = '<div class="form-group"><input type="text" name="group[]" class="form-control" value="" placeholder="Täis nimi"></div>';
        $('.form-group:last').after(html);
    });

</script>
</body>
</html>