<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?= $baseUrl ?>" />
    <title>Refract - Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="/static/img/favicon.ico"/>
    <!--Boostrap-->
    <link rel="stylesheet" type="text/css" href="/static/css/bootstrap/css/bootstrap.min.css">

    <!--Font Awesome-->
    <link rel="stylesheet" type="text/css" href="/static/css/font-awesome/fontawesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/static/css/linericons/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/static/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/static/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/static/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/static/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/static/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/static/css/login/util.css">
    <link rel="stylesheet" type="text/css" href="/static/css/login/main.css">
    <!--===============================================================================================-->
</head>
<body>