<!DOCTYPE html>
<html lang="ee">
<head>
    <title>Refract - Game</title>
    <base href="<?= $baseUrl ?>" />
    <link rel="stylesheet" type="text/css" href="/static/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/static/css/leaflet/leaflet.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <script src="/static/js/jquery/jquery.min.js"></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
    <style media="screen">
        .centerPage {
            height: 100%;
            width: 100%;
            display: flex;
            position: fixed;
            align-items: center;
            justify-content: center;
        }
        #gameMap { position: absolute; top: 0; bottom: 0; width: 100%; }
    </style>
    <div id="game-over" class="centerPage" style="display:none">
        <h1>Mäng läbi</h1>
    </div>

</head>
<body>
<div id="gameMap"></div>
<div class="modal" tabindex="-1" role="dialog" id="modalAnswer">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Küsimus</h5>
            </div>
            <div class="modal-body">
                <h3 class="mb-3" id="question"></h3>
                <div id="AnswerWord" style="display: none">
                    <input class="form-control" type="text" name="answer" placeholder="Vastus" />

                </div>
                <div id="AnswerNumber" style="display: none">
                    <input class="form-control" type="number" name="answer">
                </div>

                <div id="AnswerSelection" style="display: none">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="answerSubmit" class="btn btn-primary">Vasta</button>
            </div>
        </div>
    </div>
</div>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" ></script>
<script src="/static/js/leaflet/leaflet.js"></script>
<script src="/static/js/game/game.js"></script>
</body>
</html>