<?php
if(isset($_SESSION['tid'])) {
    header('Location: /addMember');
}
?>
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100 p-t-50 p-b-90">
            <form class="login100-form validate-form flex-sb flex-w" action="process" method="post">
					<span class="login100-form-title p-b-51">
						Refract - Login
					</span>


                <div class="wrap-input100 validate-input m-b-16" data-validate = "Puudub tiimi nimi">
                    <input class="input100" type="text" name="authName" placeholder="Tiimi nimi">
                    <span class="focus-input100"></span>
                </div>


                <div class="wrap-input100 validate-input m-b-16" data-validate = "Parool puudub">
                    <input class="input100" type="password" name="password" placeholder="Parool">
                    <span class="focus-input100"></span>
                </div>


                <div class="container-login100-form-btn m-t-17">
                    <input type="hidden" name="process" value="login">
                    <input type="hidden" name="LoginType" value="Team">
                    <input class="login100-form-btn" type="submit" value="Logi sisse" />
                </div>

            </form>
        </div>
    </div>
</div>


<div id="dropDownSelect1"></div>