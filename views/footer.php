<!--===============================================================================================-->
<script src="/static/js/jquery/jquery.min.js"></script>
<!--===============================================================================================-->
<script src="/static/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="/static/js/popper.js/esm/popper.js"></script>
<script src="/static/js/bootstrap/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="/static/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="/static/vendor/daterangepicker/moment.min.js"></script>
<script src="/static/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="/static/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="/static/js/main.js"></script>

</body>
</html>