<div class="card pt-5">
    <div class="card-header">
        Login test
    </div>
    <div class="card-body">
        <?php if (!$session->get('uid')): ?>
        <form action="process" method="post">
            <select class="custom-select" name="LoginType" required>
                <option value="" disabled selected>Login Types</option>
                <option value="Normal">Normal</option>
                <option value="Team">Team</option>
            </select>
            <div class="form-group">
                <label for="exampleInputEmail1">Username</label>
                <input type="name" class="form-control" id="exampleInputEmail1" name="authName" aria-describedby="emailHelp" placeholder="Enter email">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
            </div>
            <input type="hidden" name="process" value="login">
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        <?php else: ?>
            <h5 class="card-title">You're already logged in!</h5>
            <a href="process"><button type="submit" class="btn btn-primary">Logout</button></a>
            <h5 class="card-title mt-4">Session login info</h5>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">key</th>
                        <th scope="col">value</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($session->get() as $key => $value): ?>
                    <tr>
                        <th scope="col"><?= $key ?></th>
                        <th scope="col"><?php $route->data($value); ?></th>
                    </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        <?php endif;?>
    </div>
</div>