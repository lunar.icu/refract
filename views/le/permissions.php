<?php
$session->add('uid', 1);
$session->printAll();
$permissionTypes = $perm->permissionTypes();
$permissions = $perm->permissions();
$groups = $perm->groups();
?>

<div class="card">
    <div class="card-header">
        Lunar Engine Permissions
    </div>
    <div class="card-body">
        <h5 class="card-title">List of permissions</h5>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">permission</th>
                    <th scope="col">description</th>
                    <th scope="col">type id</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!isset($permissions[0])): ?>
                <tr>
                    <?php foreach ($permissions as $value): ?>
                        <td><?= $value ?></td>
                    <?php endforeach;?>
                </tr>
                <?php else: ?>
                    <?php foreach ($permissions as $key => $value ): ?>
                        <tr>
                            <?php foreach ($value as $data): ?>
                                <td><?= $data ?></td>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <h5 class="card-title">List of Permission types</h5>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">type name</th>
                    <th scope="col">description</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!isset($permissionTypes[0])): ?>
                    <tr>
                        <?php foreach ($permissionTypes as $value): ?>
                            <td><?= $value ?></td>
                        <?php endforeach;?>
                    </tr>
                <?php else: ?>
                    <?php foreach ($permissionTypes as $key => $value ): ?>
                        <tr>
                            <?php foreach ($value as $data): ?>
                                <td><?= $data ?></td>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <h5 class="card-title">List of groups</h5>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">group name</th>
                    <th scope="col">description</th>
                    <th scope="col">level</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!isset($groups[0])): ?>
                    <tr>
                        <?php foreach ($groups as $value): ?>
                            <td><?= $value ?></td>
                        <?php endforeach;?>
                    </tr>
                <?php else: ?>
                    <?php foreach ($groups as $key => $value ): ?>
                        <tr>
                            <?php foreach ($value as $data): ?>
                                <td><?= $data ?></td>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <h5>add group</h5>
        <form  action="process" method="post">
            <div class="form-group">
                <label for="exampleInputEmail1">Group name</label>
                <input type="text" class="form-control" name='name' id="exampleInputEmail1" aria-describedby="emailHelp"">
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Description</label>
                <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">level</label>
                <input type="number" class="form-control" name="level" id="exampleInputPassword1">
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Check me out</label>
            </div>
            <input type="hidden" name="process" value="createGroup">
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>


