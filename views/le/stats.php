<div class="card">
    <div class="card-header">
        Lunar Engine stats
    </div>
    <div class="card-body">
        <h5 class="card-title">Lunar Engine</h5>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Route</th>
                    <th scope="col">File location</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>LE Version: </td>
                    <td><?= $config->config['le']['version']?></td>
                </tr>
                <tr>
                    <td>PHP version: </td>
                    <td><?= phpversion() ?></td>
                </tr>
                <tr>
                    <td>Mysql version: </td>
                    <td><?= $db->getVersion() ?></td>
                </tr>
                <tr>
                    <td>base folder: </td>
                    <td><?= $route->getBaseFolder() ?></td>
                </tr>
                <tr>
                    <td>Route folder: </td>
                    <td><?= $route->getRouteFolder() ?></td>
                </tr>
                <tr>
                    <td>Base file</td>
                    <td><?= $route->getBasePage() ?></td>
                </tr>

                </tbody>
            </table>
        </div>
        <h5 class="card-title">Routing systems</h5>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Data</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($normalizedData as $key => $value): ?>
                <tr>
                    <td><?= $key ?></td>
                    <td><?php

                        if (is_array($value)):
                        echo '<pre>';
                        print_r($value);
                        echo '</pre>';
                        else:
                            echo $value;
                         endif?></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <br>
        <h5 class="card-title">header files</h5>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Route</th>
                    <th scope="col">File</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($route->getBefore() as $key => $value): ?>
                    <tr>
                        <td><?= $key ?></td>
                        <td><?php

                            if (is_array($value)):
                                echo '<pre>';
                                print_r($value);
                            else:
                                echo $value;
                            endif?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <br>
        <h5 class="card-title">footer files</h5>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Route</th>
                    <th scope="col">File</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($route->getAfter() as $key => $value): ?>
                    <tr>
                        <td><?= $key ?></td>
                        <td><?php

                            if (is_array($value)):
                                echo '<pre>';
                                print_r($value);
                            else:
                                echo $value;
                            endif?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <br>
        <h5 class="card-title">Exceptional files</h5>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Route</th>
                    <th scope="col">File location</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($exceptions as $key => $value): ?>
                    <tr>
                        <td><?= $key ?></td>
                        <td><?php

                            if (is_array($value)):
                                echo '<pre>';
                                print_r($value);
                            else:
                                echo $value;
                            endif?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <h5 class="card-title">Languages</h5>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Language</th>
                    <th scope="col">Language files</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach($route->getLangOptions() as $lang): ?>
                    <tr>
                        <td><?= $lang ?></td>
                        <td>
                            <?php foreach($route->getLangFiles($lang) as $files): ?>
                            <?=$files."<br>"; ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

