<html lang="ee">
<head>
    <base href="<?= $baseUrl ?>" />
    <title>LE - STATS</title>
    <link rel="stylesheet" type="text/css" href="/static/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/static/css/le.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" >
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" ></script>

</head>
<body>
<?php if ($session->get('toastr')): ?>
    <p class="toastr toastr-<?= $session->get('toastr', 'type') ?>" style="display:none"><?= $session->get('toastr', 'message'); ?></p>
    <?php $session->delete('toastr') ?>
<?php endif; ?>
<div class="container mt-5">