<div class="card mt-3 mb-5">
    <div class="card-header">
        Lunar Engine Powered by <a href="https://lunar.icu/" class="text-secondary">Lunar</a>
    </div>
</div>
</div>
<script>
    $(document).ready(function(){
        if ($(".toastr")[0]) {
            toastr.options.progressBar = true;
            toastr.options.preventDuplicates = false;
            toastr.options.closeButton = true;

            if ($(".toastr-warning")[0]) {
                var str = $( ".toastr-warning" ).text();
                toastr.warning(str);
            } else if ($(".toastr-success")[0]) {
                var str = $( ".toastr-success" ).text();
                toastr.success(str);
            } else if ($(".toastr-info")[0]) {
                var str = $( ".toastr-info" ).text();
                toastr.info(str);
            } else if ($(".toastr-error")[0]) {
                var str = $( ".toastr-error" ).text();
                toastr.error(str);
            }
        }
    });
</script>

</body>
</html>