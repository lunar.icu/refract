<!-- Page Header-->
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Tiimide nimekiri</h2>
    </div>
</header>
<section class="tables">
    <div class="container-fluid">
        <div class="card">
            <div class="card-close">
            </div>
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Tiimid</h3>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tiimi nimi</th>
                            <th>Liikmed</th>
                            <th>Mäng</th>
                            <th>Kustuta</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; foreach($game->getTeams() as $value): ;?>
                            <?php $gameid = $value['gameid'] ?>
                        <tr>
                            <td></td>
                            <td><?= $value['name']?></td>
                            <td><?= ($value['members'] != null ? $value['members'] : "-")?></td>
                            <td><?= ($value['gamename'] != null ? $value['gamename'] : "-")?></td>
                            <td><?= ($gameid  != null ? '<a href="gameList"> <i class="fas fa-external-link-alt"></i>' : '<i class="fa fa-trash text-red" aria-hidden="true"></i>')?></td>
                        </tr>
                        <?php $i++; endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</section>