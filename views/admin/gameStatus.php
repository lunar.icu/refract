<?php
if (!isset($req[1])) {
    echo "<script> location.href='${requestDir}gameList'; </script>";
}
$gameid = $req[1];
$data = $game->gameStatus($gameid);
if ($data === false) {
    echo "<script> location.href='${requestDir}gameList'; </script>";
}
$gamename = $data['gameName'];
$teams = $data['team'];
$count = $data['count'];
$ended = $data['ended'];
?>
<!-- Page Header-->
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Mäng - <?= $gamename ?></h2>
    </div>
</header>
<section class="tables">
    <div class="container-fluid">
        <div class="mb-3">
            <button type="button" class="btn btn-white shadow text-center font-weight-bold text-primary text-uppercase mb-1 mr-5" name="status">Staatus</button>
            <button type="button" class="btn btn-white shadow text-center font-weight-bold text-primary text-uppercase mb-1 mr-5" name="check">Kontroll</button>
            <button type="button" class="btn btn-white shadow text-center font-weight-bold text-primary text-uppercase mb-1 mr-5" name="result">Tulemus</button>
        </div>
        <div id="status">
            <div class="card">
                <div class="card-close">
                </div>
                <div class="card-header d-flex align-items-center">
                    <h3 class="h4">Staatus</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Tiimi nimi</th>
                                <?php for ($i= 0; $i < $count; $i++): ?>
                                <th>Punkt <?= $i +1?></th>
                                <?php endfor; ?>
                                <th>Staatus</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; foreach($teams as $key => $value):?>

                                <tr>
                                    <td></td>
                                    <td><?= $key?></td>
                                    <?php foreach ($value['p'] as $k => $v): ?>
                                    <td><?= $v['time'] !== null ? $v['time'] : "-" ?></td>
                                    <?php endforeach; ?>
                                    <td><?= $value['status']?></td>
                                </tr>
                                <?php $i++; endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="controlCheck" style="display: none;">
            <div class="card">
                <div class="card-close">
                </div>
                <div class="card-header d-flex align-items-center">
                    <h3 class="h4">Kontroll  -</h3> <small> (Automaatselt kontrollitud vastused siia ei ilmu)</small>
                </div>
                <div class="card-body">
                    <?php if($ended): ?>
                    <div class="table-responsive">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Vali tiim</label>
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <?php foreach ($teams as $k => $v): ?>
                                     <?php if($v['check'] && $v['status'] === 'Lõpetatud'): ?>
                                    <option value="<?=$k?>"><?=$k?></option>
                                    <?php endif; ?>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        <?php foreach($teams as $key => $value): ?>
                        <?php if($value['check'] === true && $value['status'] === 'Lõpetatud'): ?>
                        <div id="team_<?=$key?>">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Küsimus</th>
                                    <th>Vastus</th>
                                    <th>Mängija Vastus</th>
                                    <th>Õige</th>
                                    <th>Vale</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0;
                                foreach ($value['p'] as $k => $v ): ?>
                                    <?php
                                if (!empty($v['answer_boolean'])) {
                                    continue;
                                }
                                    ?>
                                <tr>
                                    <td></td>
                                    <td><?= $v['question']?></td>
                                    <td><?= $v['answer'] ?></td>
                                    <td><?= $v['userAnswer'] ?></td>
                                    <td><div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="<?=$v['gsid'] ?>" id="radio<?= ++$i?>" value="1">
                                            <label class="form-check-label" for="radio<?= $i?>">Õige</label>
                                        </div></td>
                                    <td><div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="<?=$v['gsid'] ?>" id="radio<?= ++$i?>" value="0">
                                            <label class="form-check-label" for="radio<?= $i?>">Vale</label>
                                        </div></td>
                                </tr>
                                <?php endforeach; ?>


                                </tbody>
                            </table>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; ?>
                        <input type="submit" class="btn btn-primary float-right" id="triggerAnswers" value="Salvesta">
                        <?php else: ?>
                        <h1>Mäng pole läbi!</h1>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="endGame" style="display: none;">
            <div class="card">
                <div class="card-close"></div>
                <div class="card-header d-flex align-items-center">
                    <h3 class="h4">Tulemus</small>
                </div>
                <div class="card-body">
                    <?php if ($ended): ?>
                    <?php else: ?>
                        <h1>Mäng pole läbi!</h1>

                    <?php endif; ?>

                </div>
            </div>
        </div>

    </div>
</section>

