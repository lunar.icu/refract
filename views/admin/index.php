
<!-- Page Header-->
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Armatuurlaud</h2>
    </div>
</header>
<!-- Dashboard Counts Section-->
<section class="dashboard-counts no-padding-bottom">
    <div class="container-fluid">
        <div class="row bg-white has-shadow">
            <!-- Item -->
            <div class="col-xl-6 col-sm-6">
                <div class="item d-flex align-items-center">
                    <div class="icon bg-violet"><i class="fas fa-map-marked-alt"></i></div>
                    <div class="title"><span>Mängud</span></div>
                    <div class="number"><strong><?= $game->countGames() ?></strong></div>
                </div>
            </div>
            <!-- Item -->
            <div class="col-xl-6 col-sm-6">
                <div class="item d-flex align-items-center">
                    <div class="icon bg-red"><i class="fas fa-users"></i></div>
                    <div class="title"><span>Tiimid</span></div>
                    <div class="number"><strong><?= $game->countTeams() ?></strong></div>
                </div>
            </div>

        </div>

    </div>

</section>