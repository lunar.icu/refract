<?php if (!isset($_SESSION['LoginType']) && $_SESSION['LoginType'] !== 'Normal') {
    header('Location: ' . $requestDir . 'login');
} ?>
<!DOCTYPE html>
<html>
<head>
    <base href="<?= $baseUrl ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Refract Admin KP</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="/static/css/bootstrap/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" type="text/css" href="/static/css/font-awesome/all.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" type="text/css" href="static/css/admin/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" type="text/css" href="static/css/admin/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" type="text/css" href="static/css/admin/custom.css">
    <!-- Toastr -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="static/img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9-->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="/static/leaflet/leaflet.css" />
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet-geosearch@3.0.0/dist/geosearch.css" />
    <link rel="stylesheet" type="text/css" href="/static/css/leaflet/Control.Geocoder.css" />

</head>
<body>
<?php if ($session->get('toastr')): ?>
    <p class="toastr toastr-<?= $session->get('toastr', 'type') ?>" style="display:none"><?= $session->get('toastr', 'message'); ?></p>
    <?php $session->delete('toastr') ?>
<?php endif; ?>
<div class="page">