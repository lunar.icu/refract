<!-- Page Header-->
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Mängude nimekiri</h2>
    </div>
</header>
<section class="tables">
    <div class="container-fluid">
        <div class="card">
            <div class="card-close">
            </div>
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Mängud</h3>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Mängu nimi</th>
                            <th>Liikmed</th>
                            <th>Staatus</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; foreach($game->gameList() as $value): ?>
                            <tr id="<?= $value['gameid'] ?>">
                                <td></td>
                                <td><a href="<?= $requestDir ?>gameStatus/<?= $value['gameid']?>"><?= $value['gameName']?></td>
                                <td><?= $value['teamName']?></td>
                                <td>none</td>
                            </tr>
                            <?php $i++; endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>