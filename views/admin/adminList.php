<!-- Page Header-->
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Admini nimekiri</h2>
    </div>
</header>
<section class="tables">
    <div class="container-fluid">
        <div class="card">
            <div class="card-close">
            </div>
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Adminid</h3>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Admini nimi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; foreach(getAdmins() as $value): ?>
                            <tr>
                                <td></td>
                                <td><?= $value['nickname']?></td>
                            </tr>
                            <?php $i++; endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</section>