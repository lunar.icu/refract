<!-- Page Header-->
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Admini loomine</h2>
    </div>
</header>
<section class="addTeam">
    <div class="container-fluid">
        <div class="card">
            <div class="card-close">
            </div>
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Sisesta admini andmed</h3>
            </div>
            <div class="card-body">
                <form class="form-horizontal" action="<?= $route->getBaseFolder() ?>process"  method="post">
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Kasutajanimi</label>
                        <div class="col-sm-9">
                            <input id="inputHorizontalSuccess" name="authName" type="text" placeholder="" class="form-control form-control-success">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Parool</label>
                        <div class="col-sm-9">
                            <input id="inputHorizontalWarning" name="password" type="password" placeholder="Parool" class="form-control form-control-warning">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-9 offset-sm-3">
                            <input type="hidden" name="LoginType" value="Normal">
                            <input type="hidden" name="process" value="register">
                            <input type="submit" value="Loo admin" class="btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>
</section>