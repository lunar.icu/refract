<?php if (isset($_SESSION['LoginType']) && $_SESSION['LoginType'] === 'Normal') {
    header('Location: index');
} ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Refract - Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="https://d19m59y37dris4.cloudfront.net/admin/1-4-6/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://d19m59y37dris4.cloudfront.net/admin/1-4-6/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="https://d19m59y37dris4.cloudfront.net/admin/1-4-6/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="https://d19m59y37dris4.cloudfront.net/admin/1-4-6/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="https://d19m59y37dris4.cloudfront.net/admin/1-4-6/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="/static/img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
<div class="page login-page">
    <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
            <div class="row">
                <!-- Logo & Information Panel-->
                <div class="col-lg-6">
                    <div class="info d-flex align-items-center">
                        <div class="content">
                            <div class="logo">
                                <h1>Refract - Login</h1>
                            </div>
                            <p></p>
                        </div>
                    </div>
                </div>
                <!-- Form Panel    -->
                <div class="col-lg-6 bg-white">
                    <div class="form d-flex align-items-center">
                        <div class="content">
                            <form action="<?= $route->getBaseFolder() ?>process" method="post" class="form-validate">
                                <div class="form-group">
                                    <input id="login-username" type="text" name="authName" required data-msg="Please enter your username" class="input-material">
                                    <label for="login-username" class="label-material">Kasutaja nimi</label>
                                </div>
                                <div class="form-group">
                                    <input id="login-password" type="password" name="password" required data-msg="Please enter your password" class="input-material">
                                    <label for="login-password" class="label-material">Password</label>
                                </div>
                                <input type="hidden" name="LoginType" value="normal">
                                <input type="hidden" name="process" value="login">
                                <input type="submit" class="btn btn-primary" name="login" title="login" value="Login">
                                <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyrights text-center">
        <p>Design by <a href="https://bootstrapious.com/p/admin-template" class="external">Bootstrapious</a>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
        </p>
    </div>
</div>
<button type="button" data-toggle="collapse" data-target="#style-switch" id="style-switch-button" class="btn btn-primary btn-sm d-none d-md-inline-block"><i class="fa fa-cog fa-2x"></i></button>

<!-- JavaScript files-->
<script src="https://d19m59y37dris4.cloudfront.net/admin/1-4-6/vendor/jquery/jquery.min.js"></script>
<script src="https://d19m59y37dris4.cloudfront.net/admin/1-4-6/vendor/popper.js/umd/popper.min.js"> </script>
<script src="https://d19m59y37dris4.cloudfront.net/admin/1-4-6/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="https://d19m59y37dris4.cloudfront.net/admin/1-4-6/vendor/jquery.cookie/jquery.cookie.js"> </script>
<script src="https://d19m59y37dris4.cloudfront.net/admin/1-4-6/vendor/chart.js/Chart.min.js"></script>
<script src="https://d19m59y37dris4.cloudfront.net/admin/1-4-6/vendor/jquery-validation/jquery.validate.min.js"></script>
<!-- Main File-->
<script src="https://d19m59y37dris4.cloudfront.net/admin/1-4-6/js/front.js"></script>
</body>
</html>
