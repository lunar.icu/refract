<!-- Main Navbar-->
<header class="header">
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
                <!-- Navbar Header-->
                <div class="navbar-header">
                    <!-- Navbar Brand --><a href="admin/" class="navbar-brand d-none d-sm-inline-block">
                        <div class="brand-text d-none d-lg-inline-block"><strong>Refract</strong></div>
                        <div class="brand-text d-none d-sm-inline-block d-lg-none"><strong>Refract</strong></div></a>
                    <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
                </div>
                <!-- Navbar Menu -->
                <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                    <!-- Languages dropdown
                    <li class="nav-item dropdown"><a id="languages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link language dropdown-toggle"><img src="static/img/flags/16/GB.png" alt="English"><span class="d-none d-sm-inline-block">English</span></a>
                        <ul aria-labelledby="languages" class="dropdown-menu">
                            <li><a rel="nofollow" href="#" class="dropdown-item"> <div class="flag-icon flag-icon-ee mr-2" style="height:16px"></div> German</a></li>
                            <li><a rel="nofollow" href="#" class="dropdown-item"> <img src="static/img/flags/16/FR.png" alt="English" class="mr-2">French</a></li>
                        </ul>
                    </li>-->
                    <!-- Logout    -->
                    <li class="nav-item"><a href="/process" class="nav-link logout"> <span class="d-none d-sm-inline">Logi välja</span><i class="fa fa-sign-out"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<div class="page-content d-flex align-items-stretch">
    <!-- Side Navbar -->
    <nav class="side-navbar">
        <!-- Sidebar Navidation Menus-->
        <ul class="list-unstyled">
            <li><a href="admin/"> <i class="fas fa-home"></i>Armatuurlaud</a></li>
            <span class="heading">TIIMIDE SEKTSIOON</span>
            <li><a href="admin/teams"> <i class="fas fa-users"></i> Tiimide nimekiri</a></li>
            <li><a href="admin/addTeams"> <i class="fas fa-user-plus"></i> Tiimide loomine</a></li>
            <span class="heading">Mängu sektsioon</span>
            <li><a href="admin/gameList"><i class="fas fa-map-marked-alt"></i>Mängude nimekiri</a></li>
            <li><a href="admin/addGame"><i class="fas fa-plus"></i> Mängu loomine</a></li>
            <span class="heading">Admini sektsioon</span>
            <li><a href="admin/adminList"><i class="fas fa-user-alt"></i>Admini nimekiri</a></li>
            <li><a href="admin/admin"><i class="fas fa-plus"></i> Admini loomine</a></li>
            
        </ul>
    </nav>
    <div class="content-inner">