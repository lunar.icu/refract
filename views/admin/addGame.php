<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Mängu loomine</h2>
    </div>
</header>
<?php $teams = (array) getAvailableTeams(); ?>
<section id="gameName">
    <div class="container-fluid">
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <h3 class="h4">Mängu nimi</h3>
        </div>
        <div class="card-body">
                <div class="form-group">
                    <input type="text" id="valGameName" class="form-control" placeholder="Mängu nimi" required>
                </div>
        </div>

    </div>
        <button type="button" class="nextStep btn btn-info float-right">Edasi <i class="fas fa-arrow-right"></i></button>
    </div>
</section>

<section id="teams" style="display: none">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Vali tiimid</h3>
            </div>
            <div class="card-body">
                <?php if (sizeof($teams) >= 2): ?>
                    <?php foreach($teams as $team): ?>
                        <div class="i-checks">
                            <input  type="checkbox" value="<?php echo $team['tid']; ?>" id="teamCheck<?php echo $team['tid']; ?>" class="checkbox-template">
                            <label for="teamCheck<?php echo $team['tid']; ?>"><?= $team['name'] ?></label>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <h2 class="text-danger">Sul peab vähemalt 2 vaba tiimi olema!</h2>
                <?php endif; ?>




            </div>

        </div>
        <button type="button" class="nextStep btn btn-info float-right">Edasi <i class="fas fa-arrow-right"></i></button>
        <button type="button" class="prevStep btn btn-danger"><i class="fas fa-arrow-left"></i> Tagasi </button>

    </div>
</section>
<style>
    #map-container {
        position: relative;
        height: 400px;
        width: 100%;
    }

    #map {
        position: relative;
        height: inherit;
        width: inherit;
    }
</style>
<section id="maps" style="display: none">

    <div class="container-fluid">
        <div id="map-container">
            <div id="map"></div>
            <!-- Modal-->
            <div id="createQuestion" style="position: fixed" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" class="modal fade text-left">
                <div role="document" class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 id="exampleModalLabel" class="modal-title">Pukti küsimuse loomine</h4>
                            <button type="button" data-dismiss="modal" aria-label="Close" class="cordsQuit close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div id="modal-clone" class="modal-body">
                            <p></p>
                            <form>
                                <div class="form-group">
                                    <label>Küsimus</label>
                                    <input type="text" id="valQuestion" placeholder="Küsimus" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Vastuse tüüp</label>
                                    <select id="AnswerTypes" name="account" class="form-control mb-3">
                                        <option value="AnswerWord">Sõnaline</option>
                                        <option value="AnswerNumber">Numbriline</option>
                                        <option value="AnswerSelection">Valikuline</option>
                                    </select>



                                </div>
                                <div id="AnswerType">
                                    <div id="AnswerWord">
                                        <small class="help-block-none">*Sõnalise vastuse tüübi puhul peab orgniseerija vastust kontrollima*</small>
                                        <br>
                                        <label for="inputWord">Vastus</label>
                                        <input type="text" placeholder="Vastus" id="inputWord" class="form-control" required><small class="help-block-none">*Selleks et kontrollija ei pea pärast uurima mis vastus oli*</small>
                                    </div>
                                    <div id="AnswerNumber" style="display: none">
                                        <small class="help-block-none">*Numbrilise vastuse tüübi puhul tehakse automaatne kontroll*</small>
                                        <br>
                                        <label for="inputNumber">Vastus</label>
                                        <input type="number" placeholder="Vastus" id="inputNumber" class="form-control" required><small class="help-block-none">*Selleks et kontrollija ei pea pärast uurima mis vastus oli*</small>
                                    </div>
                                    <div id="AnswerSelection" style="display: none">
                                        <small class="help-block-none">*Valik vastuse puhul tehakse automaatne kontroll*</small><br>
                                        <small class="help-block-none">Vajuta checkboxile, kui vastus on õige</small>


                                        <div id="SelectionData">
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">
                                                        <div class="form-check">
                                                            <input class="form-check-input position-static" type="checkbox" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Valik vastus">
                                            </div>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">
                                                        <div class="form-check">
                                                            <input class="form-check-input position-static" type="checkbox"   />
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Valik vastus">
                                            </div>

                                        </div>




                                        <button type="button" id="SelectionDataClone" class="btn btn-success"><i class="fas fa-plus"></i> Lisa juurde</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="cordsQuit btn btn-secondary">Sulge</button>
                            <button type="button" id="saveQuestion" class="btn btn-primary">Loo küsimus</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-3">
            <button type="button" id="create-game"  class="btn btn-info float-right"><i class="fas fa-plus"></i> Loo Mäng</button>
            <button type="button" class="prevStep btn btn-danger"><i class="fas fa-arrow-left"></i> Tagasi </button>
        </div>
        <div class="card mt-4">
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Punktid</h3>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="QuestionsRecords" class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Küsimus</th>
                            <th>Vastuse Tüüp</th>
                            <th>Vastused</th>
                            <th>Kustuta</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="SelectionDataCopy" style="display: none">
        <div class="copyField input-group mb-2" >
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <div class="form-check">
                        <input class="form-check-input position-static" type="checkbox" />
                    </div>
                </div>
            </div>
            <input type="text" class="form-control" placeholder="Valik vastus"><i style="cursor: pointer;" class="deleteOption text-danger mt-2 ml-3 fas fa-times"></i>
        </div>
    </div>
</section>


