<!-- Page Footer-->
<footer class="main-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <p><a href="https://lunar.icu" target="_blank"> Project Lunar &copy; 2016-2020</a></p>
            </div>
            <div class="col-sm-6 text-right">
                <p>Design by <a href="https://bootstrapious.com/p/admin-template" class="external">Bootstrapious</a></p>
                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
        </div>
    </div>
</footer>
</div>
</div>
</div>
<!-- JavaScript files-->
<script src="static/js/jquery/jquery.min.js"></script>
<script src="static/js/popper.js/umd/popper.min.js"> </script>
<script src="static/js/bootstrap/bootstrap.min.js"></script>
<script src="static/js/jquery.cookie/jquery.cookie.js"> </script>
<script src="static/js/jquery-validation/jquery.validate.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="/static/js/leaflet/leaflet.js"></script>
<script src="/static/js/geoman/leaflet-geoman.min.js"></script>
<script src="/static/js/leaflet/Control.Geocoder.js"></script>
<script src="static/js/adminMap.js"></script>

<link rel="stylesheet" href="static/css/geoman/leaflet-geoman.css" />



<!-- Main File-->
<script src="static/js/custom/front.js"></script>
<script>
    $(document).ready(function(){
        if ($(".toastr")[0]) {
            toastr.options.progressBar = true;
            toastr.options.preventDuplicates = false;
            toastr.options.closeButton = true;
            var str = null;
            if ($(".toastr-warning")[0]) {
                str = $( ".toastr-warning" ).text();
                toastr.warning(str);
            } else if ($(".toastr-success")[0]) {
                str = $( ".toastr-success" ).text();
                toastr.success(str);
            } else if ($(".toastr-info")[0]) {
                str = $( ".toastr-info" ).text();
                toastr.info(str);
            } else if ($(".toastr-error")[0]) {
                str = $( ".toastr-error" ).text();
                toastr.error(str);
            }
        }
    });
    $(document).ready(function () {
        var url = window.location;
        $('ul.list-unstyled a[href="'+ url +'"]').parent().addClass('active');
        $('ul.list-unstyled a').filter(function() {
            return this.href === url;
        }).parent().addClass('active');
    });


</script>
<script>
    var IDs = [];
    var curretid = 0;
    var bolContinue = true;
    $('body').find("section").each(function(){ IDs.push(this.id); });
    var currentSection = IDs[curretid];
    var nextSection = null;
    $(".nextStep").on('click', function(event){
        if ($('#gameName')) {
            if ($('#gameName input').val().trim() != 0) {
                bolContinue = true;
            } else {
                $('#gameName input').addClass('is-invalid');
                bolContinue = false;
            }

        }
        if ($('#teams').is(":visible")) {
            const teams = [];
            $('#teams input:checked').each(function() {
                teams.push($(this).attr('value'));
            });
            console.log(teams);
            if (teams.length <= 1) {
                toastr.error("Sa pead valima vähemalt 2 tiimi!");
                bolContinue = false;
            } else {
                bolContinue = true;
            }
        }
        if (bolContinue) {
            $('input:visible').removeClass('is-invalid');
            if ( curretid < IDs.length-1) {
                $( "#" + currentSection).toggle( "slide" );
                curretid = curretid + 1;
                nextSection = IDs[curretid];
                $( "#" + nextSection).toggle( "slide" );
                currentSection = nextSection;
                if (curretid == IDs.length-1) {
                    adminMap();
                }
            }
        }

    });

    $(".prevStep").on('click', function(event){
        $( "#" + currentSection).toggle( "slide" );
        curretid = curretid - 1;
        nextSection = IDs[curretid];
        $( "#" + nextSection).toggle( "slide" );
        currentSection = nextSection;


    });
    (function () {
        var previous;

        $("#AnswerTypes").on('focus', function () {
            previous = this.value;
        }).change(function() {
            $("#" + previous).toggle("slide");
            $("#" + this.value).toggle("slide");
            previous = this.value;
        });
    })();
    $(function() {
        $("button[name='status']").on('click', function(event) {
            event.preventDefault();
            $( "#status" ).css('display', 'block');
            $( "#controlCheck" ).css('display', 'none');
            $( "#endGame" ).css('display', 'none');
        });
        $("button[name='check']").on('click', function(event) {
            event.preventDefault();
            $( "#status" ).css('display', 'none');
            $( "#controlCheck" ).css('display', 'block');
            $( "#endGame" ).css('display', 'none');
        });
        $("button[name='result']").on('click', function(event) {
            event.preventDefault();
            $( "#status" ).css('display', 'none');
            $( "#controlCheck" ).css('display', 'none');
            $( "#endGame" ).css('display', 'block');
        });
    })
    $('#triggerAnswers').on('click', function () {
        var div = $("div[id^=team_]");
        var data = []
        var radiosBtns = div.find("input[type='radio']:checked");
        radiosBtns.map(function (val, i) {
            data.push({
                'gsid': i.name,
                'answer_boolean': i.value
            });
        })
        const url = window.location.origin;
        const dataURL = url + '/ajax';
        $.ajax({
            url: dataURL,
            type: "POST",
            dataType: 'json',
            data: {
                'data': data,
                'process': 'updateAnswer'
            },
            success: function(data) {
                location.reload();
            },
        });
    });

</script>
</body>
</html>