<?php
if(isset($_POST['process'])) {
    $post = $_POST['process'];
    if(function_exists($post)) {
        $post();
    } else {
        die('no request');
    }

} else {
    header('Location: /404');
}
function gameStart() {
    global $game, $session;
    $tid = $session->get('tid');
    $gameid = $_POST['gameid'];
    $result = $game->start($tid, $gameid);
    echo $result;
}
function getMarkers() {
    global $session, $game;
    $tid = $session->get('tid');
    $data = $game->getGame($tid);
    echo json_encode($data);
}

function save() {
    global $session, $game;
    $tid = $tid = $session->get('tid');
    $gameid = $_POST['gameid'];
    $answer = $_POST['answer'];
    $answerBoolean = isset($_POST['answerBoolean']) ? (boolean) $_POST['answerBoolean']: null;
    $glid = $_POST['glid'];
    $result = $game->save($tid, $gameid,$glid, $answer, $answerBoolean);
    echo $result;
}

function gameEnd() {
    global $session, $game;
    $tid = $session->get('tid');
    $gameid = $_POST['gameid'];
    $game->end($tid, $gameid);
}

function updateAnswer() {
    global $game;
    $data = $_POST['data'];
    $result = $game->updateAnswer($data);
    echo $result;

}