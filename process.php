<?php
if(isset($_POST['process'])) {
    $post = $_POST['process'];
    if(function_exists($post)) {
        $post();
    } else {
        die('no request');
    }

} else {
    logout();
}

function login() {
    global $session, $lm;
    $loginType = ucfirst($_POST['LoginType']);
    $data['authName'] = $_POST['authName'];
    $data['password'] = $_POST['password'];
    $result = $lm->request($loginType, 'loginCheck', $data);
    if ($result) {
        $session->add('LoginType', $loginType);
        foreach ($lm->loginConfig[$loginType]['requiredData'] as $key => $value) {
            $session->add($key, $result[$value]);
        }
        //$ltname = Login Type name table
        $ltname = $lm->loginConfig[$loginType]['name'];
        $name = $result[$ltname];
        $session->add('toastr', 'success', 'type');
        $session->add('toastr', "Tere $name!", 'message');
    } else {
        $session->add('toastr', 'error', 'type');
        $session->add('toastr', 'Wrong login data', 'message');
    }
}

function register() {
    global $lm;
    $loginType = ucfirst($_POST['LoginType']);
    $data['authname'] = $_POST['authName'];
    $data['password'] = $_POST['password'];
    $result = $lm->request($loginType, 'register', $data);
}

function addMembers() {
    global $lm, $session;
    if (!$_SESSION['tid']) {
        header('Location: /index');
    }
    $tid = $_SESSION['tid'];
    $members = implode(',', $_POST['group']);
    $data = ['tid' => $tid, 'members' => $members];
    $result = $lm->request('Team', 'addMembers', $data);
    if ($result) {
        $_SESSION['members'] = $members;
        $session->add('toastr', 'success', 'type');
        $session->add('toastr', "Tiimi liikmed on listaud!", 'message');

        header('Location: /game');
    } else {
        $session->add('toastr', 'error', 'type');
        $session->add('toastr', "Midagi läks valesti", 'message');
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }
}

function createGroup() {
    global $perm, $session;
    $name = $_POST['name'];
    $desc = $_POST['description'];
    $level = $_POST['level'];
    $result = $perm->setGroup($name, $desc, $level);
    if ($result) {
        $session->add('toastr', 'success', 'type');
        $session->add('toastr', "Successfully created $name group", 'message');
    } else {
        $session->add('toastr', 'error', 'type');
        $session->add('toastr', "Group '$name' already exists", 'message');
    }
}
/*
function createPermission() {
    global $perm, $session;
    $permission = $_POST['permission'];
    $desc = $_POST['description'];
    $ptid = $_POST['ptid'];


}*/

function createTeam() {
    global $session, $lm;
    $name = $data['name']  = $_POST['name'];
    $data['password']  = $_POST['password'];
    $result = $lm->request('Team', 'register', $data);
    if ($result) {
        $session->add('toastr', 'success', 'type');
        $session->add('toastr', "Successfully created $name team", 'message');
    } else {
        $session->add('toastr', 'error', 'type');
        $session->add('toastr', "Team '$name' already exists", 'message');

    }
}

function createGame() {
    global $route, $post, $game;

    $polygon = $_POST['startFin'];
    $gameName = html_entity_decode($_POST['gamename']);
    $markers = $_POST['markers'];
    $teamIds = $_POST['teams'];
    $return = $game->create($polygon, $markers, $teamIds, $gameName);
}

function logout() {
    global $session;
    $session->clear();
    $session->add('toastr', 'success', 'type');
    $session->add('toastr', 'You have been successfully logged out!', 'message');
}




//$route->data($_SERVER);
header('Location: '.$_SERVER['HTTP_REFERER']);