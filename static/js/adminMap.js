function adminMap() {
    let shape = null;
    let cords = null;
    let layer = null;
    let shape_id = null;
    const currentZoon = 16;
    let startingPlace = [];
    let markerRecords = [];
    const options = {
        position: 'topleft',
        drawCircleMarker: false,
        drawRectangle: false,
        drawPolyline: false,
        drawCircle: false,
        cutPolygon: false,

    }

    const map = L.map('map').setView([51.505, -0.09], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/satellite-streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibHVtaW5lbyIsImEiOiJjazZzNml2dDMwY2x2M25udm9pa2FuOTQ1In0.rNCPXVoOg-dG0cCn2DrilA'
    }).addTo(map);
    var geocoder = L.Control.geocoder({
        defaultMarkGeocode: false
    })
        .on('markgeocode', function(e) {
            map.setView(e.geocode.center, currentZoon);
        })
        .addTo(map);
    map.pm.addControls(options);

    // Kui marker või polygon on loodud
    // listen to when drawing mode gets disabled
    map.on('pm:dragstart', function(e) {
        console.log(e)
    });
// listen to when drawing mode gets disabled
    map.on('pm:drag', function(e) {
        console.log(e)
    });
    // listen to when drawing mode gets disabled
    map.on('pm:dragend', function(e) {
        console.log(e)
    });
    map.on('pm:create', e => {
        console.log('/// Creating mode')
        shape = e.shape;
        shape_id = e.layer._leaflet_id;
        // listen to changes on the new layer
        e.layer.on('pm:edit', function(x) {
            edit(x);
        });
        if (shape === "Marker") {
            layer = e.layer;
            cords = layer._latlng;
            layer.bindPopup("test");
            $('#createQuestion').modal('show');
            disableMap();
            map.pm.disableDraw(shape);
            $('#SelectionDataClone').on('click', function () {
                console.log('e');
                $('#SelectionData .input-group').first().clone()
                    .find("input:text").val("").end()
                    .find("input:checkbox").prop('checked', false).end()
                    .addClass('optionCopy')
                    .appendTo( "#SelectionData" );
                $(this).off('click');
            });
            $('.cordsQuit').on('click', function () {
                map.removeLayer(e.layer);
                $(this).off('click');
            });
            $('#saveQuestion').on('click', function() {
                let question = null;
                let answerTypeText = null;
                let answerTypeVal = null;
                let answer = 0;
                answerTypeVal = $("#createQuestion #AnswerTypes").val();
                question = $("#createQuestion #valQuestion").val().trim();
                answerTypeText = $("#createQuestion #AnswerTypes option:selected").text();


                if ($("#AnswerWord:visible").is(":visible")) {
                    answer = $("#createQuestion #inputWord").val().trim();
                }

                if ($("#AnswerNumber").is(":visible")) {
                    answer = $("#createQuestion #inputNumber").val().trim();
                }

                if ($("#AnswerSelection").is(":visible")) {
                    answer = checkOptionValues();
                    console.log('lenght' + answer.length);
                }

                if (question.length === 0) {
                    $("#createQuestion #valQuestion").addClass("is-invalid");
                }
                if (answerTypeVal !== "AnswerSelection") {
                    if (answer == null) {
                        $("#createQuestion #AnswerType input:visible").addClass("is-invalid");
                    }
                } else {

                }

                if (question.length !== 0 && (answerTypeVal === "AnswerSelection" || answer.length !== 0)) {

                    markerRecords[shape_id] = {
                        lat: cords.lat,
                        lng: cords.lng,
                        question: question,
                        answerTypeText: answerTypeText,
                        answerTypeVal: answerTypeVal,
                        answer: answer
                    }
                    refreshTable();
                    $('#modal-clone').find("input:text, input[type='number']").val('').removeClass('is-invalid');
                    $('#modal-clone').find("input:checkbox").prop('checked', false);
                    enableMap();
                    $("div.optionCopy").remove();
                    $('#createQuestion').modal('hide');
                    layer.bindPopup(question);
                    $('#createQuestion #saveQuestion').off('click');

                }
                function checkOptionValues() {
                    let values = [];
                    let selectValue;
                    let selectBoolean;
                    let selectBooleanCounter = 0;
                    $('#SelectionData input').each(function(i)
                    {
                        switch ($(this).attr('type'))
                        {
                            case 'text':
                                selectValue = $(this).val();
                                break;
                            case 'checkbox' :
                                selectBoolean = $(this).is(":checked");
                                if (selectBoolean) {
                                    selectBooleanCounter++;
                                }
                                break;
                        }
                        if (i % 2 === 1) {
                            if (selectValue.length !== 0) {
                                values.push({
                                    answer: selectValue,
                                    boolean: selectBoolean
                                })
                            }
                        }
                    });
                    if (values.length < 2) {
                        toastr.error("Valikuid peab olema rohkem kui 1!");
                        return null;
                    } else if (selectBooleanCounter === 0) {
                        toastr.error("Õigeid vastuseid peab olema rohkem kui 0!");
                        return null;
                    }
                    console.log("booleanCounter:" + selectBooleanCounter)

                    return values;
                }


            });
        }
        if (shape === "Polygon") {
            console.log(e)
            let layer = e.layer;
            let latLngs = layer._latlngs;
            startingPlace = latLngs[0];
            console.log(startingPlace);
            map.pm.disableDraw('Polygon');
        }
    });

    //When Polygon or marker is edited
    map.on('pm:edit', e => {
        console.log("//// Edit mode");
        console.log(e);
    });

    //When Polygon or marker is moved

    map.on('pm:globaleditmodetoggled', e => {
        console.log('/// toggle edit')
        console.log(e);
    });
    //When Polygon or marker is deleted
    map.on('pm:remove', e => {
        console.log('/// Remove mode');
        console.log(e);
        const layer = e.layer;
        const layerId = layer._leaflet_id;
        const shape = e.shape;
        if (shape === "Marker") {
            delete markerRecords[layerId];
            refreshTable()
        } else {
            startingPlace = null;
        }


    });

    function edit(e) {
        console.log(e);
        const shape = e.shape;
        const layer = e.layer;
        const layerID = layer._leaflet_id;
        if (shape === "Marker") {
            const latLng = layer._latlng;
            markerRecords[layerID]['lat'] = latLng.lat;
            markerRecords[layerID]['lng'] = latLng.lng;
        }
        if (shape === "Polygon") {
            const latLng = layer._latlngs;
            startingPlace = latLng;
        }
        refreshTable();

    }


    function refreshTable() {
        console.log(markerRecords);
        if (markerRecords != null) {
            $('#QuestionsRecords tbody').empty();
            for(const v in markerRecords) {
                if (markerRecords[v].answerTypeVal === "AnswerSelection") {
                    var output = '';
                    $.each(markerRecords[v].answer, function(key, value) {
                        var isCorrect;
                        if (value.boolean) {
                            isCorrect = "Tõene";
                        } else {
                            isCorrect = "Väär";
                        }
                        output += value.answer + " (" + isCorrect + ")</br>" ;
                    });
                    answer = output;
                } else {
                    answer = markerRecords[v].answer;
                }
                $('#QuestionsRecords tbody').append('<tr id="' + v + '"><td></td><td>' + markerRecords[v].lat + '</td><td>'+ markerRecords[v].lng +'</td><td>' + markerRecords[v].question + '</td><td style="display: none">' + markerRecords[v].answerTypeVal + '</td> <td class="dummy">' + markerRecords[v].answerTypeText + '</td> <td>' + answer + '</td> <td class="text-center"><i style="cursor: pointer" class="fas fa-trash text-danger delete"></i></td></tr>');
            }
        }
    }
    $('table').on('click', '.delete', function() {
        const layerId = $(this).closest("tr").attr('id');
        map.removeLayer(map._layers[layerId]);
        delete markerRecords[layerId];
        refreshTable();
    });

    $('#create-game').on('click', function (e){
        e.preventDefault();
        let doneBoolean = true;
        const gameName = $('#gameName input').val().trim();
        const teams = [];
        $('#teams input:checked').each(function() {
            teams.push($(this).attr('value'));
        });
        console.log("gamename:" + gameName);
        console.log("teams ///", teams);
        const markerCounter = Object.keys(markerRecords).length;
        if (markerCounter <= 1) {
            toastr.error("Markerid peab olema vähemalt 2!");
            doneBoolean = false;
        }
        console.log(startingPlace);
        if (startingPlace === null) {
            toastr.error("Starti ala puudub!");
            doneBoolean = false;
        }
        if (doneBoolean) {
            const url = window.location.origin;
            const dataRecords = [];
            const fixedMarkerRecords = [];
            $.map(markerRecords, function (i, item) {
                if (i !== undefined) {
                    fixedMarkerRecords.push(i);
                }
            });
            dataRecords['MarkerData'] = fixedMarkerRecords;
            dataRecords['startFin'] = startingPlace;
            dataRecords['gameName'] = gameName;
            dataRecords['teams'] = teams;
            if (typeof startingPlace =='object') {
                startingPlace = JSON.stringify(startingPlace);
            }


            $.ajax({
                url: url +'/process',
                type: 'POST',
                data: {
                    'startFin': startingPlace,
                    'markers': fixedMarkerRecords,
                    'teams': teams,
                    'gamename': gameName,
                    'process': 'createGame'
                },
                async: true,
                beforeSend: function() {
                    console.log("loading");
                },
                complete: function() {
                    location.assign(url + '/admin/gamelist');
                    console.log("Done with this shit");

                },

            }).done(function() {
                console.log("success");
                //
            })
                .fail(function() {
                    console.log("error");
                });
        }
    });

    function disableMap() {
        map.dragging.disable();
        map.touchZoom.disable();
        map.doubleClickZoom.disable();
        map.scrollWheelZoom.disable();
        map.boxZoom.disable();
        map.keyboard.disable();
        if (map.tap) map.tap.disable();
        document.getElementById('map').style.cursor='default';
    }
    function enableMap() {
        map.dragging.enable();
        map.touchZoom.enable();
        map.doubleClickZoom.enable();
        map.scrollWheelZoom.enable();
        map.boxZoom.enable();
        map.keyboard.enable();
        if (map.tap) map.tap.enable();
        document.getElementById('map').style.cursor='grab';
    }
}
//map.removeLayer(e.layer); - good shit will delete what i want