(function($){
    const url = window.location.origin;
    const dataURL = url + '/ajax';
    let markers = [];
    let startFin = null;
    let started = false;
    let ended = false;
    let userLocation = null;
    let countTrueAnswers = 0;
    let rightAnswer;
    let answer = '';
    let marker = null;
    let userCircle = null;
    let ajaxData = false;
    let gameid = null;
    let bolSetup = false;
    let startCords = null;
    let polygon;
    const maxDistance = 10;
    console.log(dataURL);
    const map = L.map('gameMap').fitWorld()
    $.ajaxSetup({
        url: dataURL,
        type: "POST",
        dataType: 'json',

    });
    $.ajax(dataURL, {
        data: {
            'process': 'getMarkers'
        },
        success: function(data) {
            console.log('//// Getting game data');
            console.log(data);
            markers = data.markers;
            startFin = data.startFin;
            started = data.started;
            ended = data.ended;
            gameid = data.gameid;
            ajaxData = true;
            if (ended) {
                window.location.replace(url + "/finished");
            }
            setup();
            startGame();
        },
    });


    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/satellite-streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibHVtaW5lbyIsImEiOiJjazZzNml2dDMwY2x2M25udm9pa2FuOTQ1In0.rNCPXVoOg-dG0cCn2DrilA'
    }).addTo(map);

    map.locate({setView: true, maxZoom: 16, watch: true, enableHighAccuracy:true});
    map.locate({ maxZoom: 100, enableHighAccuracy:true, watch:true});

    function setup() {
        console.log('//// Checking if game is already setup ');
        if (bolSetup === false) {
            console.log('//// Setting up the game');
            startCords = $.map(startFin, function(value, index) {
                return [[value.lat, value.lng]];
            });
            console.log('//// Creating start and finish area');

            polygon = L.polygon(startCords, {color: 'red'}).addTo(map);
            map.fitBounds(polygon.getBounds());
            bolSetup = true;
            if (marker === null ) {
                setMarker(markers[0].lat, markers[0].lng);
            }
        }
    }

    function checkInside() {
        if (userLocation === null) {
            return false;
        }
        const inside = polygon.getBounds().contains(userLocation.latlng);
        if(inside) {
            return true;
        } else {
            return false;
        }
    }

    function setMarker() {
        if (marker !== null) {
            console.log('//// Removing old marker data')
            map.removeLayer(marker)
            $('#' + markers[0].answerType).toggle();
            markers.shift();
        }
        if (markers.length !== 0) {
            console.log('//// Creating new marker with questions')
            $(':input').val('');

            marker = L.marker([markers[0].lat, markers[0].lng]).addTo(map);
            $("#question").prepend(markers[0].question);

            if (markers[0].answerType === "AnswerSelection") {
                console.log('dsa')
                const result = createSelection();
                $("#AnswerSelection ").html(result);

            } else {

            }
            $('#' + markers[0].answerType).toggle();
        } else {

        }

    }

    function userDistance() {
        console.log(userLocation);
        const from = userLocation.latlng;
        console.log(marker);
        const to = marker.getLatLng();
        const distance = to.distanceTo(from);
        if (maxDistance >= distance) {
            $('#modalAnswer').modal({backdrop: 'static', keyboard: false});
        }

    }

    function createSelection() {
        const array =  $.parseJSON(markers[0].answerSelection);
        let selections = '';
        $.map(array, function (val, i) {
            if (val.boolean === 'true') {
                countTrueAnswers++;
            }
            selections += "<div class=\"form-check\"><input class=\"form-check-input\" type=\"checkbox\" value=\"" + val.answer + "\" id=\"defaultCheck" + i + "\">\n" +
                "  <label class=\"form-check-label\" for=\"defaultCheck" + i + "\">"+ val.answer +"</label></div>"
        });
        return selections;
    }

    $("#answerSubmit").on('click', function (e) {
        const answers =  $.parseJSON(markers[0].answerSelection);
        const answerType = markers[0].answerType;
        switch (answerType) {
            case 'AnswerSelection':
                const userSelections= [];

                let countUserTrue = 0;
                let countUserFalse = 0;

                $('#'+ answerType + ' input[type=checkbox]:checked').each(function(i) {
                    const userAnswer = $(this).val();
                    $.each(answers, function(i, v) {
                        if (v.answer === userAnswer) {
                            if(v.boolean === 'true') {
                                countUserTrue++;
                            } else {
                                countUserFalse++;
                            }
                        }
                    });
                    userSelections[i] = $(this).val();
                })
                if (countUserFalse !== 0 || countTrueAnswers > countUserTrue) {
                    rightAnswer = false;
                } else if (countUserTrue === countTrueAnswers) {
                    rightAnswer = true;
                }
                answer = userSelections.toString()
                if (userSelections.length === 0) {
                    toastr.error('Sa pead vähemalt 1 vastuse valima!');
                } else {
                    $('#modalAnswer').modal('hide');
                }
                break;
            default:
                answer = $('#' + answerType + " input").val().trim();
                if (answer === '') {
                    toastr.error('Vastus on tühi');
                } else {
                    $('#modalAnswer').modal('hide');
                }
                if (answerType === 'AnswerNumber') {
                    rightAnswer = answer === markers[0].answerNumber;
                    console.log(rightAnswer);
                }
                console.log(answer);

                break;
        }
        save();
        setMarker();
    });



    function onLocationFound(e) {
        //showUserMarker(e); // for debugging
        if (ajaxData) {
            userLocation = e;
            if (!started) {
                console.log('//Checking if user is in start area');
                if (checkInside()) {
                    startGame();
                }

            } else if(started && markers.length === 0) {
                console.log('//Checking if user is in finish area');
                if (checkInside()) {
                    gameEnd();
                }
            }
            else {
                if (marker === null) {
                    setMarker(markers[0].lat, markers[0].lng);
                }
            }
            if (started && markers.length !== 0) {
                userDistance();
            }
        }
    }

    function save() {
        console.log('//Saving Answerrs');
        $.ajax(dataURL, {
            type: "POST",
            dataType: 'json',
            data: {
                'process': 'save',
                'gameid': gameid,
                'answer': answer,
                'glid': markers[0].glid,
                'answerBoolean': rightAnswer
            },
            success: function(data) {
                rightAnswer = null;
            },
        });
    }

    function startGame() {
        if (!started) {
            $.ajax( {
                data: {
                    'process': 'gameStart',
                    'gameid': gameid
                },
                success: function(data) {
                    started = true;
                },
            });
        }
    }

    function showUserMarker(e) {
        if (userCircle != null) {
            map.removeLayer(userCircle)
        }
        userCircle = L.circle(e.latlng, {
            color: 'red',
            fillColor: 'red',
            fillOpacity: 0.5,
            radius: maxDistance,
        }).addTo(map);
        if (started && marker !== null) {
            console.log('pop');
            console.log(userLocation);
            const from = e.latlng;
            console.log(marker);
            const to = marker.getLatLng();
            const distance = to.distanceTo(from);

            userCircle.bindPopup("" +distance).openPopup();
        }
    }

    function gameEnd() {
        $.ajax({
            data: {
                'process': 'gameEnd',
                'gameid': gameid
            },
            success: function (data) {
                window.location.replace(url + "/finished");
            },
        });
    }
    map.on('locationfound', onLocationFound);
})(jQuery);
