function map() {
    var dumb;
    var layer;
    var question;
    var answer;
    var obj;
    var markerLayer;
    var defaultModalHtml = null;
    var selectCopy;
    var answerTypeText;
    var answerTypeVal;
    var markerID;
    var arrMarkers = {};
    var data = {};
    var tempMarker;
    var marker;
    var questionType;
    var startFin = [];
    var lat;
    var Data;
    var map = L.map('map').setView([51.505, -0.09], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'sk.eyJ1IjoibHVtaW5lbyIsImEiOiJja2V1cWRpcTgzeGZ3MnRwY216ZDViNWNyIn0.zhUZC3rM-yoTRvCGohww0Q',
    }).addTo(map);
    // FeatureGroup is to store editable layers
    var drawnItems = new L.FeatureGroup();
    map.addLayer(drawnItems);
    var drawControl = new L.Control.Draw({
        draw: {
            polyline: false,
            circlemarker: false,
            circle: false,
            rectangle: false,
        },
        edit: {
            featureGroup: drawnItems
        }
    });
    map.addControl(drawControl);
    map.on(L.Draw.Event.CREATED, function(e) {
        question = null;
        answer = null;
        type = null;
        layer = null;
        type = e.layerType;
        layer = e.layer;
        markerLayer = e.layer;
        if (type == 'marker' && map.hasLayer(layer) == false) {
            tempMarker = layer._latlng;
            markerID = layer._leaflet_id;
            console.log(tempMarker);
            lat = tempMarker.lat;
            lng = tempMarker.lng;

            $('#createQuestion').modal('show');
            if (defaultModalHtml === null) {
                defaultModalHtml = $('#createQuestion').children().clone(true, true);

            }

            console.log(defaultModalHtml);
            $('#SelectionDataClone').click(function(e) {
                selectCopy = $('#SelectionDataCopy').children().clone();
                $('#SelectionData').append(selectCopy);
            });
            $(document).on('click', ".deleteOption", function() {
                $(this).closest('.copyField').remove();
            });

            $('#saveQuestion').click(function(e) {
                question = null;
                answerTypeText = null;
                answerTypeVal = null;
                answer = null;
                answerTypeVal = $("#createQuestion #AnswerTypes").val();
                question = $("#createQuestion #valQuestion").val().trim();
                answerTypeText = $("#createQuestion #AnswerTypes option:selected").text();


                if ($("#AnswerWord:visible").is(":visible")) {
                    console.log("///Word Anwsers////");
                    answer = $("#createQuestion #inputWord").val().trim();
                }

                if ($("#AnswerNumber").is(":visible")) {
                    console.log("///Number Anwsers////");
                    answer = $("#createQuestion #inputNumber").val().trim();
                }

                if ($("#AnswerSelection").is(":visible")) {
                    console.log("///Option Anwsers////");
                    answer = checkOptionValues();
                }
                console.log("Question: " + question);
                console.log("Answer Type: " + answerTypeVal);
                console.log("Answer: " + answer);
                console.log('Question length:' + question.length );
                if (question.length == 0) {
                    console.log('ma käisin kogemata siin');
                    $("#createQuestion #valQuestion").addClass("is-invalid");
                }
                if (answerTypeVal !== "AnswerSelection") {
                    if (answer.length == 0) {
                        $("#createQuestion #AnswerType input:visible").addClass("is-invalid");
                    }
                } else {

                }

                if (question.length != 0 && (answerTypeVal == "AnswerSelection" || answer.length != 0)) {
                    arrMarkers[markerID] = {
                        question: question,
                        questionType: answerTypeVal,
                        answer: answer,
                        cords: {
                            lat: lat,
                            lng: lng
                        }
                    };

                    console.log(arrMarkers);
                    drawnItems.addLayer(markerLayer);
                    var output = "";
                    if (answerTypeVal == "AnswerSelection") {
                        $.each(answer, function(key, value) {
                            output += value.answer + " (" + value.boolean + ")</br>" ;
                        });
                        answer = output;
                    }

                    $('#QuestionsRecords tbody').append('<tr id="' + markerLayer._leaflet_id + '"><td></td><td>' + lat + '</td><td>'+ lng +'</td><td>' + question + '</td><td style="display: none">' + answerTypeVal + '</td> <td class="dummy">' + answerTypeText + '</td> <td>' + answer + '</td></tr>');
                    $('#createQuestion').html(defaultModalHtml);
                    $('#createQuestion').modal('hide');
                    layer.bindPopup(question);
                    $('#createQuestion #saveQuestion').off('click');
                    //console.log(drawnItems);
                }
            });
            /*$('#createQuestion').on('hidden.bs.modal', function(e) {
                $(this).html(defaultModalHtml);
            });*/
        }
        function checkOptionValues() {
            var values = [];
            var selectValue;
            var selectBoolean;
            var selectBooleanCounter = 0;
            $('#SelectionData input').each(function(i)
            {
                switch ($(this).attr('type'))
                {
                    case 'text':
                        selectValue = $(this).val();
                        break;
                    case 'checkbox' :
                        selectBoolean = $(this).is(":checked");
                        if (selectBoolean) {
                            selectBooleanCounter++;
                        }
                        break;
                }
                if (i % 2 == 1) {
                    if (selectValue.length != 0) {
                        values.push({
                            answer: selectValue,
                            boolean: selectBoolean
                        })
                    }
                }
            });
            console.log(values)
            if (values.length < 2) {
                alert("Valikuid peab olema rohkem kui 1!");
            } else if (selectBooleanCounter == 0) {
                alert("Õigeid vastuseid peab olema rohkem kui 0!");
            }
            console.log("booleanCounter:" + selectBooleanCounter)
        return values;
        }


    });
}
